import {Component} from '@angular/core';
import {PwaService} from '../../services/pwa/pwa.service';

@Component({
  selector: 'app-install',
  templateUrl: './install.component.html',
  styleUrls: ['./install.component.scss']
})
export class InstallComponent {

  constructor(public Pwa: PwaService) {
  }

  installPwa(): void {
    this.Pwa.runInstall();
  }

}
