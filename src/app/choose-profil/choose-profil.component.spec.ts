import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseProfilComponent } from './choose-profil.component';

describe('ChooseProfilComponent', () => {
  let component: ChooseProfilComponent;
  let fixture: ComponentFixture<ChooseProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
