import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  endpoint = 'http://localhost:3000/api/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<any> {
    return this.http.get(this.endpoint + 'users').pipe(
      map(this.extractData));
  }

  getUser(id): Observable<any> {
    return this.http.get(this.endpoint + 'users/' + id).pipe(
      map(this.extractData));
  }

  addUser(user): Observable<any> {
    console.log(user);
    return this.http.post<any>(this.endpoint + 'users', JSON.stringify(user), this.httpOptions).pipe(
      tap((userRes) => console.log(`added user w/ id=${userRes.id}`)),
      catchError(this.handleError<any>('addUser'))
    );
  }

  updateUser(id, user): Observable<any> {
    return this.http.put(this.endpoint + 'users/' + id, JSON.stringify(user), this.httpOptions).pipe(
      tap(_ => console.log(`updated user id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'users/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted user id=${id}`)),
      catchError(this.handleError<any>('deleteUser'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
