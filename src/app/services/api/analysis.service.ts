import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {

  endpoint = 'http://localhost:3000/api/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getAnalysises(): Observable<any> {
    return this.http.get(this.endpoint + 'analysis').pipe(
      map(this.extractData));
  }

  getAnalysis(id): Observable<any> {
    return this.http.get(this.endpoint + 'analysis/' + id).pipe(
      map(this.extractData));
  }

  addAnalysis(product): Observable<any> {
    console.log(product);
    return this.http.post<any>(this.endpoint + 'analysis', JSON.stringify(product), this.httpOptions).pipe(
      tap((productRes) => console.log(`added analysis w/ id=${productRes.id}`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }

  updateAnalysis(id, product): Observable<any> {
    return this.http.put(this.endpoint + 'analysis/' + id, JSON.stringify(product), this.httpOptions).pipe(
      tap(_ => console.log(`updated analysis id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteAnalysis(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'analysis/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted analysis id=${id}`)),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
