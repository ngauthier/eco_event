import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  endpoint = 'http://localhost:3000/api';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getAllQuestions(): Observable<any> {
    return this.http.get(this.endpoint + 'questions').pipe(
      map(this.extractData));
  }

  getQuestions(id): Observable<any> {
    return this.http.get(this.endpoint + 'questions/' + id).pipe(
      map(this.extractData));
  }

  addQuestions(questions): Observable<any> {
    console.log(questions);
    return this.http.post<any>(this.endpoint + 'questions', JSON.stringify(questions), this.httpOptions).pipe(
      tap((questionsRes) => console.log(`added questions w/ id=${questionsRes.id}`)),
      catchError(this.handleError<any>('addQuestions'))
    );
  }

  updateQuestions(id, questions): Observable<any> {
    return this.http.put(this.endpoint + 'questions/' + id, JSON.stringify(questions), this.httpOptions).pipe(
      tap(_ => console.log(`updated questions id=${id}`)),
      catchError(this.handleError<any>('updateQuestions'))
    );
  }

  deleteQuestions(id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'questions/' + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted questions id=${id}`)),
      catchError(this.handleError<any>('deleteQuestions'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
}
