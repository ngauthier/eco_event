import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectpageComponent } from './connectpage.component';

describe('ConnectpageComponent', () => {
  let component: ConnectpageComponent;
  let fixture: ComponentFixture<ConnectpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
