import { Component, OnInit } from '@angular/core';
import {UserData} from '../../model/user-data';
import {AuthService, FacebookLoginProvider} from 'angular5-social-login';

@Component({
  selector: 'app-connectpage',
  templateUrl: './connectpage.component.html',
  styleUrls: ['./connectpage.component.scss']
})

export class ConnectpageComponent implements OnInit {

  user: UserData;

  constructor(private socialAuthService: AuthService) {
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData: UserData) => {
        this.user = userData;
        /* TODO: Appel symfony our enregistrement en db */
        // Now sign-in with userData
      }
    );
  }

  ngOnInit(): void {
  }

}
