import {Questionnaire} from './questionnaire';

export class User {
  id: number;
  name: string;
  username: string;
  password: string;
  facebookId: number;
  survey: Array<Questionnaire>;
}
