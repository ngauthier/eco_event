import {Questionnaire} from './questionnaire';

export class Event {
  id: number;
  name: string;
  facebookId: number;
  date: Date;
  questionnaires: Array<Questionnaire>;
}
