/**
 * Classe representant un user depuis l'authO
 */
export class UserData {

  email: string;
  id: string;
  image: string;
  name: string;
  provider: string;
  token: string;

}
