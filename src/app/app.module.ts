import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';



import {AuthServiceConfig, FacebookLoginProvider, SocialLoginModule} from 'angular5-social-login';
import {ConnectpageComponent} from './connection/connectpage/connectpage.component';
import {LineComponent} from './test/line/line.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EventComponent } from './event/event.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import { from } from 'rxjs';
import { BurgerMenuComponent } from './commun/burger-menu/burger-menu.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { InformationComponent } from './connection/information/information.component';
import { ChooseProfilComponent } from './choose-profil/choose-profil.component';
import { ExplorerComponent } from './explorer/explorer.component';



// Configs
export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('710458432654841')
      },
    ]
  );
}


@NgModule({
  declarations: [
    AppComponent,
    LineComponent,
    ConnectpageComponent,
    EventComponent,
    BurgerMenuComponent,
    InformationComponent,
    ChooseProfilComponent,
    ExplorerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    SocialLoginModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule
  ],
  providers: [
      {
        provide: AuthServiceConfig,
        useFactory: getAuthServiceConfigs
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
